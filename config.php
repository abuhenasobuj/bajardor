<?php
/**
 * Created by PhpStorm.
 * User: abuhenasobuj
 * Date: 8/19/17
 * Time: 4:42 PM
 */

$SITE_URL = "http://www-bajardor.a3c1.starter-us-west-1.openshiftapps.com";
//$SITE_URL = "http://bajardor.dev";
$API_URL = "http://app.bajardor.com";
$GOOGLE_SITE_SEARCH = "bajardor.com";

$total_category = range(1, 8); // Total valid category id 1-8
$total_product = range(1, 59); // Total valid product id 1-59

function clean($string) {
	$string = str_replace(array('/','(',')',' ','','--'), array('-',' ','','-',' ','-'), $string); // Replaces all spaces with hyphens.
	return $string;
}
function titleclean($string) {
	$string = str_replace(array('/','(',')',' ','','--','-'), array('-',' ','','-',' ','-',' '), $string); // Replaces all spaces with hyphens.
	return $string;
}
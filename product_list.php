<?php
/**
 * Created by PhpStorm.
 * User: abuhenasobuj
 * Date: 8/16/17
 * Time: 8:39 AM
 */

$json = file_get_contents(''.$API_URL.'/api.php?cat_id='.$_GET[cid].'');

$data = json_decode($json);

echo '<section class="bajardor_app">
    <div class="container_bajardor">
        <div class="row">';

echo '<div class="title">
                <div class="col-lg-12 col-sm-12 title-bg">বিভাগ: ';
echo titleclean($_GET[category]);
echo '</div>
                <div class="col-lg-6 col-sm-6 col-xs-6 sm-border-1" style="width: 33.33%;"></div>
                <div class="col-lg-6 col-sm-6 col-xs-6 sm-border-2" style="width: 33.33%;"></div>
                <div class="col-lg-6 col-sm-6 col-xs-6 sm-border-3" style="width: 33.33%;"></div>
                <div class="clearfix"></div>
            </div>';

foreach ($data->BajardorApp as $string) {
    echo "<div class=\"product-listview-by-cat\">
                <div class=\"col-lg-6 col-sm-6 product-content\"><a href=\"".$SITE_URL."/product/" . $string->nid . "/" . clean($string->news_heading) . ".html\"><h3>" . $string->news_heading . "</h3></a><p>$string->news_date</p></div>
                <div class=\"clearfix\"></div>
            </div>";
}

echo '</div>
        <div class="col-lg-12 pagenation-center">
        </div>
    </div>

</section>';
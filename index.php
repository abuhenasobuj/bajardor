<?php
error_reporting(1);
session_start(1);

include 'config.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php

        // TODO: Title

        $uri = $_SERVER["REQUEST_URI"];

        $parts = parse_url($uri);
        parse_str($parts['query'], $query);

        if (isset($_GET['product'])) {
            echo titleclean($_GET['product']) ." - বাজারদর";
        } elseif (isset($_GET['category'])) {
            echo titleclean($_GET['category']) ." - বাজারদর";
        }
        else{
            echo 'বাজারদর';
        } ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="বাজারদর.কম - নিত্য প্রয়োজনীয় সকল পণ্যের বর্তমান বাজারদর।">
    <meta property="og:image" content="<?php echo $SITE_URL; ?>/assets/img/logo.png">

    <link rel="shortcut icon" href="<?php echo $SITE_URL; ?>/favicon.ico">

    <!-- Loading Bootstrap -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="<?php echo $SITE_URL; ?>/assets/js/html5shiv.js"></script>
    <script type="text/javascript" src="<?php echo $SITE_URL; ?>/assets/js/respond.min.js"></script>
    <![endif]-->

    <link type="text/css" href="<?php echo $SITE_URL; ?>/assets/css/app.min.css" rel="stylesheet" media="all">
    <link type="text/css" rel="stylesheet" href="<?php echo $SITE_URL; ?>/assets/css/style.css">

    <link rel="alternate" href="<?php echo $SITE_URL; ?>" hreflang="bn-BD" />

    <meta name="keywords" content="">

    <meta name="description"
          content="নিত্য প্রয়োজনীয় সকল পণ্যের বর্তমান বাজারদর।">
</head>
<body class="home">
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-sm-2 col-xs-6 header-logo"><a href="<?php echo $SITE_URL; ?>" class="logo"><img
                            src="<?php echo $SITE_URL; ?>/assets/img/logo.png" alt="বাজারদর"></a>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-3 col-sm-2 msg">
                <h2>
                </h2>
            </div>
            <div class="col-lg-7 col-sm-8">
                <div class="row">

                    <div class="col-lg-7 col-sm-7 search-box">
                        <!--<form target="_blank" onsubmit="return before_search();" action="http://google.com/search" method="get">
                            <input type="hidden" id="search_q" name="q" value="" />
                            <input tabindex="1" type="search" id="search_input"  placeholder="Search"/>
                        </form>-->

                        <form method="get" action="http://google.com.bd/search" target="_blank" onsubmit="return before_search();">
                            <input type="hidden" id="search_q" name="q" value="" />
                            <div class="form-group">
                                <div class="input-group"> <span class="input-group-btn">
                  <button class="btn" type="submit"> <span class="fui-search"></span> </button>
                  </span>
                                    <input class="form-control" id="search_input" placeholder="গুগল এর সাহায্যে খুঁজুন" type="search">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<?php

if (in_array($_GET['cid'], $total_category)) {

    // TODO: Category

    require 'product_list.php';

} elseif (in_array($_GET['pid'], $total_product)) {

    // TODO: Product Details

    require 'product_details.php';

} elseif ($_GET['error'] === 'not-found') {

	// TODO: Product Details

		require '404.php';

} else {

    //TODO: Default (Home) Page

    require 'categories.php';
}

?>

<footer>
    <p>© <a href="<?php echo $SITE_URL; ?>">বাজারদর</a></p>
    <p><a href="#"> Help </a> - <a href="#">  Send feedback </a> - <a href="#"> Privacy & Policy </a></p>
</footer>

<script type="text/javascript">
    function before_search(){
        var searchVal = 'site:<?php echo $GOOGLE_SITE_SEARCH; ?> ' + document.getElementById('search_input').value;
        document.getElementById('search_q').value = searchVal;
        return true;
    }
</script>

<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="<?php echo $SITE_URL; ?>/assets/js/app-min-compressed.js"></script>

</body>
</html>
